package com.mavendc.cq.coffeescript

import org.osgi.framework.BundleActivator
import org.osgi.framework.BundleContext
import org.slf4j.Logger
import org.slf4j.LoggerFactory
/**
 * Created with IntelliJ IDEA.
 * User: John Dorrance
 * Date: 10/26/13
 * Time: 7:35 PM
 */
class Activator implements BundleActivator{


    private Logger log = LoggerFactory.getLogger(this.class)

    @Override
    void start(BundleContext bundleContext) throws Exception {
        log.info("Coffee Support started");

    }

    @Override
    void stop(BundleContext bundleContext) throws Exception {
        log.info("Coffee Support stopped")
    }
}
