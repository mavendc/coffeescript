package com.mavendc.cq.coffeescript

import org.apache.felix.scr.annotations.Component
import org.apache.felix.scr.annotations.Service

/**
 * Created with IntelliJ IDEA.
 * User: John Dorrance
 * Date: 10/26/13
 * Time: 2:49 PM
 */

interface CoffeeScriptCompiler{
    String getJSfromCoffee(String js, Boolean isBare, Boolean isIced) throws CoffeeScriptCompileException
}

@Service
@Component(immediate = true)
class CoffeeScriptConpilerImpl implements CoffeeScriptCompiler{

    public CoffeeScriptConpilerImpl(){}

    @Override
    String getJSfromCoffee(String js, Boolean isBare,Boolean isIced) {

        Collection<Option> options = new LinkedList<Option>();
        if(isBare) options.add(Option.BARE);
        if(isIced) options.add(Option.ICED);
        new CompilerImpl(options).compile(js);

    }

}



