package com.mavendc.cq.coffeescript
import org.apache.felix.scr.annotations.Component
import org.apache.felix.scr.annotations.Service
import org.mozilla.javascript.Context
import org.mozilla.javascript.JavaScriptException
import org.mozilla.javascript.Scriptable
/**
 * Created with IntelliJ IDEA.
 * User: John Dorrance
 * Date: 10/26/13
 * Time: 2:29 PM
 */

interface Compiler{
    String compile(String source) throws CoffeeScriptCompileException
}

@Service
@Component(immediate = true)
class CompilerImpl implements  Compiler{

    def Scriptable glbScope
    def Options opts
    def String compilerFileName = "coffee-script.js"
    public static final String ENCODING = "UTF-8"


    public CompilerImpl() {
        this(Collections.<Option>emptyList());
    }

    public CompilerImpl(Collection<Option> options) throws IOException, UnsupportedEncodingException{

        if(options.contains(Option.ICED)) compilerFileName = "iced-coffee-script.js"

        InputStream inputStream = getClass().classLoader.getResourceAsStream("com/mavendc/cq/coffeescript/${compilerFileName}")
            try {

                Reader rdr = new InputStreamReader(inputStream, ENCODING);

                try {

                    Context cntxt = Context.enter();
                    cntxt.setOptimizationLevel(-1)

                    try {

                        glbScope = cntxt.initStandardObjects()
                        cntxt.evaluateReader(glbScope, rdr, compilerFileName, 0, null)

                    } finally { Context.exit()}
                } finally { rdr.close()}
            } finally { inputStream.close() }

        this.opts = new Options(options);
    }
    @Override
    public String compile (String source) throws CoffeeScriptCompileException {
        Context cntxt = Context.enter()
        try {

            Scriptable cmpScope = cntxt.newObject(this.glbScope)
            cmpScope.setParentScope(this.glbScope)
            cmpScope.put("coffeeScriptSource", cmpScope, source)

            try {

                (String)cntxt.evaluateString(cmpScope,"CoffeeScript.compile(coffeeScriptSource, ${opts.javaScriptOptions});","CoffeeScriptCompiler", 0, null)

            } catch (JavaScriptException e) {
                throw new CoffeeScriptCompileException(e)
            }
        } finally { Context.exit();}
    }
}

class CoffeeScriptCompileException extends Exception {
    CoffeeScriptCompileException (JavaScriptException e) {super(e.value.toString(), e);}
}

public enum Option { BARE,ICED}

class Options {

    def String javaScriptOptions

    public Options(Collection<Option> options) {
        javaScriptOptions = String.format("{bare: %b}", options.contains(Option.BARE))
    }

}
