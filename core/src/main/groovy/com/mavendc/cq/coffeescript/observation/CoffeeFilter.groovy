package com.mavendc.cq.coffeescript.observation

import com.day.cq.widget.ClientLibrary
import com.day.cq.widget.HtmlLibraryManager
import com.mavendc.cq.coffeescript.CoffeeScriptCompiler
import com.mavendc.cq.coffeescript.sling.CoffeeSupport
import com.mavendc.cq.coffeescript.utils.CoffeeUtils
import org.apache.felix.scr.annotations.Activate
import org.apache.felix.scr.annotations.Component
import org.apache.felix.scr.annotations.Reference
import org.apache.felix.scr.annotations.Service
import org.apache.sling.api.resource.Resource
import org.apache.sling.api.resource.ResourceResolver
import org.apache.sling.api.resource.ResourceResolverFactory
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.jcr.Node
import javax.jcr.Session
/**
 * Created with IntelliJ IDEA.
 * User: John Dorrance
 * Date: 10/30/13
 * Time: 8:50 PM
 */
public interface CoffeeFilter {

    void addCoffeeRendition(String path)
    void updateCoffeeRendition(String path)
    void removeCoffeeRendition(String path)

}
@Service
@Component(immediate = true)
class CoffeeFilterImpl implements CoffeeFilter{

    @Reference
    CoffeeScriptCompiler coffeeScriptCompiler

    @Reference
    ResourceResolverFactory resolverFactory

    @Reference
    HtmlLibraryManager htmlLibraryManager

    private Logger log = LoggerFactory.getLogger(this.class)

    @Override
    void addCoffeeRendition(String path) {
        log.trace("adding coffee rendition for ${path}")
        usingResourceResolver(path, livesInClientLib, [clearChild,saveFile] )
    }


    @Override
    void updateCoffeeRendition(String path) {
        log.trace("updating coffee rendition for ${path}")
        usingResourceResolver(path, livesInClientLib, [clearChild,saveFile] )
    }

    @Override
    void removeCoffeeRendition(String path) {
        log.trace("removing coffee rendition for ${path}")
        usingResourceResolver(path, livesInClientLib, [clearChild] )
    }

    @Activate
    public void start(){
        log.info "Coffee Support started"
        usingResourceResolver("Starting Bundle", {x,y->true}, [parseClientLibs] )
    }

   private def usingResourceResolver = {String path, Closure<Boolean> outerCheck, List<Closure> operations ->
        log.trace "using resource resolver"
        ResourceResolver admResolver = resolverFactory.getAdministrativeResourceResolver()
        try{
            if(outerCheck(path,admResolver)) {
                operations.each{Closure c ->
                    c.call path,admResolver
                }
            }
        }catch(Exception e){
            log.error("Exception in updating coffee rendition", e);
        }finally{
            if(admResolver)admResolver.close()
            log.trace "resource resolver closed"
        }
    }

   private def parseClientLibs = {String info, ResourceResolver resolver ->
        log.info "Coffee Filter --> parsing through client libraries (${info})"
        def getpaths = {
            def largecoffees = []
            htmlLibraryManager.getLibraries().values().toArray().clone().each{ClientLibrary c ->
                Resource clientLibRes = resolver.getResource(c.path)
                def smallcoffees = []
                CoffeeUtils.filterRecursively(clientLibRes, CoffeeSupport.COFFEE_EXTENSION, smallcoffees).each{Resource found ->
                    largecoffees.add(found?.path)
                }
            }
            largecoffees
        }
        getpaths().each{ updateCoffeeRendition(it) }
    }
    private def saveFile = {String path, ResourceResolver adminResolver ->

        final Session adminSession = adminResolver.adaptTo(Session.class)
        Resource res = adminResolver.getResource(path)

        assert res.isResourceType("nt:file")
        assert res.parent

        log.debug "saving resource @ original path ${res?.path}"

        Node fileNode = adminSession.getNode(res.parent.path)?.addNode(res.getName() + CoffeeSupport.JS_EXTENSION, "nt:file")

        fileNode.addNode ("jcr:content", "nt:resource").with{Node n->
            String sourceJS = CoffeeUtils.convertStreamToString(res?.adaptTo(InputStream.class))
            n.setProperty "jcr:mimeType", CoffeeSupport.JS_MIME_TYPE
            n.setProperty "jcr:encoding", CoffeeSupport.JS_ENCODING
            n.setProperty "jcr:data", coffeeScriptCompiler.getJSfromCoffee(sourceJS, true, false)
            n.setProperty "jcr:lastModified", System.currentTimeMillis()
            log.debug "${path} created"
        }
        adminSession.save()
    }

    def clearChild = {String path , ResourceResolver adminResolver ->
        log.trace "clearing child for  ${path}"
        final Session adminSession = adminResolver.adaptTo(Session.class)
        String renderedPath = path + ".js"
        if(adminSession.itemExists(renderedPath)){
            adminSession.removeItem(renderedPath)
            log.debug "Removed item at ${renderedPath} exist"
        }else{
            log.debug "Item at ${renderedPath} does not exist"
        }
        adminSession.save()
    }

     def livesInClientLib = {String path, ResourceResolver adminResolver ->
        log.trace "checking if ${path} is in a clientlib"

        final Session adminSession = adminResolver.adaptTo(Session.class);
        Node cofNode = adminSession.getNode(path)
        log.trace "coffee node resolved ${cofNode?.path}"
        if(CoffeeUtils.findClientLibNode(cofNode)){
            log.debug "${path} is in a clientlib"
            return true
        }
        log.trace "no clientlib parent found for ${cofNode?.path}"
       return false
    }

}