package com.mavendc.cq.coffeescript.observation
import com.mavendc.cq.coffeescript.sling.CoffeeSupport
import org.apache.felix.scr.annotations.Component
import org.apache.felix.scr.annotations.Property
import org.apache.felix.scr.annotations.Reference
import org.apache.felix.scr.annotations.Service
import org.apache.sling.api.SlingConstants
import org.osgi.service.event.Event
import org.osgi.service.event.EventConstants
import org.osgi.service.event.EventHandler
import org.slf4j.Logger
import org.slf4j.LoggerFactory
/**
 * Created with IntelliJ IDEA.
 * User: John Dorrance
 * Date: 10/30/13
 * Time: 5:41 PM
 */
@Service(value = EventHandler.class)
@Component(immediate = true)
@Property(name = EventConstants.EVENT_TOPIC, value = [SlingConstants.TOPIC_RESOURCE_ADDED,SlingConstants.TOPIC_RESOURCE_CHANGED,SlingConstants.TOPIC_RESOURCE_REMOVED])
class CoffeeListener implements EventHandler{

    private Logger log = LoggerFactory.getLogger(this.class)

    @Reference
    CoffeeFilter coffeeFilter

    @Override
    void handleEvent(Event event) {
        final String propPath = (String) event.getProperty(SlingConstants.PROPERTY_PATH);
        final String propResType = (String) event.getProperty(SlingConstants.PROPERTY_RESOURCE_TYPE);
        if(propPath.endsWith(CoffeeSupport.COFFEE_EXTENSION) && "nt:file".equals(propResType)){
            log.trace("handling coffee file - event topic (${event.topic}")
            try{

                switch(event.topic){
                    case SlingConstants.TOPIC_RESOURCE_REMOVED:
                        log.trace("resource removed")
                        coffeeFilter.removeCoffeeRendition(propPath)

                    case SlingConstants.TOPIC_RESOURCE_ADDED:
                        log.trace("resource added")
                        coffeeFilter.addCoffeeRendition(propPath)

                    case SlingConstants.TOPIC_RESOURCE_CHANGED:
                        log.trace("resource changed")
                        coffeeFilter.updateCoffeeRendition(propPath)
                }


            }catch(Exception e){
                log.error("Error in coffee processing",e);
            }
        }
    }
}

