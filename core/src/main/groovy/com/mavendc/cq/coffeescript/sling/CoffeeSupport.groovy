
package com.mavendc.cq.coffeescript.sling

import com.mavendc.cq.coffeescript.CoffeeScriptCompiler
import com.mavendc.cq.coffeescript.utils.CoffeeUtils
import org.apache.felix.scr.annotations.*
import org.apache.sling.api.SlingHttpServletRequest
import org.apache.sling.api.SlingHttpServletResponse
import org.apache.sling.api.servlets.SlingSafeMethodsServlet
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.servlet.ServletException
import javax.servlet.http.HttpServletResponse

/**
 * Created with IntelliJ IDEA.
 * User: John Dorrance
 * Date: 10/26/13
 * Time: 2:49 PM
 */

@Service
@Component(immediate = true)

@Properties([
    @Property(name= "sling.servlet.methods",value=["GET"]),
    @Property(name="sling.servlet.resourceTypes", value=["nt:file"]),
    @Property(name="sling.servlet.extensions",value=["js"])
])

class CoffeeSupport extends SlingSafeMethodsServlet {

    public static final SAFE_SELECTOR = "safe"
    public static final ICED_SELECTOR = "iced"
    public static final COFFEE_EXTENSION = ".coffee"
    public static final JS_EXTENSION = ".js"
    public static final JS_ENCODING = "UTF-8"
    public static final JS_MIME_TYPE = "application/javascript"
    public static final COMPILE_JOB_TOPIC = "com/mavendc/cq/coffeescript/eventing/compilejob"

    @Reference
    CoffeeScriptCompiler coffeeScriptCompiler

    private Logger log = LoggerFactory.getLogger(this.class)

    @Override
    protected void doGet(final SlingHttpServletRequest request,final SlingHttpServletResponse response) throws ServletException,IOException {

        PrintWriter out = response.writer

        if(request?.resource?.path?.endsWith(COFFEE_EXTENSION)){

            boolean isBare = !request?.requestPathInfo?.selectors?.contains(SAFE_SELECTOR);
            boolean isIced = request?.requestPathInfo?.selectors?.contains(ICED_SELECTOR)
            response.setContentType(JS_MIME_TYPE)
            String sourceJS = CoffeeUtils.convertStreamToString(request?.resource?.adaptTo(InputStream.class))

            try{
                coffeeScriptCompiler.getJSfromCoffee(sourceJS,isBare,isIced).chars.each {char c -> out.append(c)}
            } catch(Exception e){

                out.write(e.message)
                log.error("Error Parsing coffeescript from $request.resource.path")
                response.sendError(HttpServletResponse.SC_BAD_REQUEST)

            }

        }else{
            response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE)
            out.write(CoffeeUtils.convertStreamToString(request?.resource?.adaptTo(InputStream.class)))
        }
        out.close()
    }

}
