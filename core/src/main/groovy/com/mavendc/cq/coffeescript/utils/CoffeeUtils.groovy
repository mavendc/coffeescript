package com.mavendc.cq.coffeescript.utils
import com.mavendc.cq.coffeescript.sling.CoffeeSupport
import org.apache.sling.api.resource.Resource
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.jcr.Node
/**
 * Created with IntelliJ IDEA.
 * User: John Dorrance
 * Date: 10/30/13
 * Time: 9:32 PM
 */
 class CoffeeUtils {

    public static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is,CoffeeSupport.JS_ENCODING).useDelimiter("\\A")
        if(s.hasNext()) return s.hasNext() ? s.next() : ""
    }

    static Node findClientLibNode(Node node) {
        Logger log = LoggerFactory.getLogger(this.class)
        if (node?.getPath() ==~ "^/apps/.*") {
            String nodeType = node?.getProperty("jcr:primaryType")?.getString()
            if(nodeType.contains("cq:ClientLibraryFolder")){
                log.trace("node is a clientlib (${node.path}");
                return node
            }else{
                log.trace("node is not a clientlib (${node.path}");
                return findClientLibNode(node?.parent)
            }
        }else{
            log.trace("reached the end of our apps (${node.path}");
        }
        return null
    }
     static List<Resource> filterRecursively(Resource res, String extension,List<Resource> result){
         def children = res.listChildren()
         for(Resource child in children){
             if(child?.path?.endsWith(CoffeeSupport.COFFEE_EXTENSION)){
                 result.add(child)
             }
             filterRecursively(child,extension,result)
         }
         return result
     }
}
