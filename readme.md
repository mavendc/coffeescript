Features

* 'On the fly' compilation of .coffee files, with sling selectors for 'safe', and 'iced - compilation modes
* CQ5 Clientlibs are 'watched' - and dynamically compiled. 
* All coffee sources placed inside of a clientlib will be pre-compiled and cached
	
	
This is open source under the MIT license. Please fork and help me out with the effort!

